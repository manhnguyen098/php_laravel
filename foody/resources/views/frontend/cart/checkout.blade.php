@extends('frontend.master.master')
<!-- END nav -->
@section('title','Đặt Hàng')
@section('header','Đặt Hàng')
@section('content')
<main class="site-main shopping-cart">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('getCart')}}">Giỏ hàng</a></li>
                <li class="breadcrumb-item active" aria-current="page">Đơn hàng của bạn</li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            @if(Session::has('thongbao'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>
                        {{Session::get('thongbao')}}
                        </strong>
                </div>
            </div>
            @endif @if(!empty(session('cart')))
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        Thông tin khách hàng
                    </div>
                    <div class="card-body">

                        <form method="POST" role="form" style="width: 100%;margin-top: 0" action="{{route('postCheckOut')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="">Họ tên*</label>
                                <input type="text" class="form-control" id="" placeholder="Nhập họ và tên" name="name"> @if($errors->has('name'))
                                <div class="help-block alert-danger">
                                    {!! $errors->first('name')!!}
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Giới tính</label>
                                <div class="form-inline">
                                    <input type="radio" class="form-control" id="" checked="checked" name="gender" value="Nam">
                                    <span style="margin-right: 50px;">Nam</span>
                                    <input type="radio" class="form-control" id="" name="gender" value="Nữ"> <span>Nữ</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Email*</label>
                                <input type="text" class="form-control" id="" placeholder="Nhập email" name="email"> @if($errors->has('email'))
                                <div class="help-block alert-danger">
                                    {!! $errors->first('email')!!}
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Địa chỉ*</label>
                                <input type="text" class="form-control" id="" placeholder="Nhập địa chỉ" name="address"> @if($errors->has('address'))
                                <div class="help-block alert-danger">
                                    {!! $errors->first('address')!!}
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Số điện thoại*</label>
                                <input type="text" class="form-control" id="" placeholder="Nhập số điện thoại" name="phone"> @if($errors->has('phone'))
                                <div class="help-block alert-danger">
                                    {!! $errors->first('phone')!!}
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Ghi chú</label>
                                <textarea name="note"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Hình thức thành toán</label>
                                <div class="form-inline">
                                    <input type="radio" class="form-control" id="" checked="checked" name="payment_method" value="COD"> <span style="margin-right: 50px;">Thanh toán khi giao hàng</span>
                                    <input type="radio" class="form-control" id="" name="payment_method" value="ATM"> <span>Chuyển khoản</span>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ĐẶT HÀNG</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        Thông tin sản phẩm
                    </div>
                    <div class="card-body">
                        <form class="form-cart" style="border: none;margin-top: 0">
                            <div class="table-cart ">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="tb-image">Ảnh</th>
                                            <th class="tb-product">Tên sản phẩm</th>
                                            <th class="tb-qty">Số lượng</th>
                                            <th class="tb-total">Tổng tiền</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach(session('cart') as $it)
                                        <tr>
                                            <td class="tb-image"><img src="{{asset('backend/image/' . $it['image'])}}" alt="" width="30px"></td>
                                            <td class="tb-product">
                                                <div class="product-name"><a href="#">{{$it['name']}}</a></div>
                                            </td>
                                            <td class="tb-total">
                                                <div class="price">
                                                    <?php
                                                    if ($it['sale_price']>0)
                                                    echo number_format($it['sale_price']);
                                                    else
                                                    echo number_format($it['price']);
                                                    ?>
                                                </div>
                                            </td>
                                            <td class="tb-total">
                                                <span class="price">
                                                    <?php
                                                    if ($it['sale_price']>0)
                                                    echo number_format($it['sale_price']*$it['quantity']);
                                                    else
                                                    echo number_format($it['price']*$it['quantity']);
                                                    ?>
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4">
                                                <h1>Tổng số tiền: {{number_format(session('total'))}}</h1></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

        </div>
    </div>
</main>
@endsection
@extends('frontend.master.master')
<!-- END nav -->
@section('title','Đặt Hàng')
@section('header','Đặt Hàng')
    
@section('content')
<main class="site-main shopping-cart">
            <div class="container">
                <ol class="breadcrumb-page">
                <li><a href="{{ route('getIndexDiscover')}}">Thực đơn </a></li>
                    <li class="active"><a href="">Shopping Cart</a></li>
                </ol>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                    	<?php 
                         if (!(empty($cart))) { ?>
                        <div class="form-cart">
                            <div class="table-cart ">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="tb-image">Ảnh</th>
                                            <th class="tb-product">Tên sản phẩm</th>
                                            <th class="tb-price">Đơn giá</th>
                                            <th class="tb-price">Giá khuyến mãi</th>
                                            <th class="tb-qty">Số lượng</th>
                                            <th class="tb-total">Tổng</th>
                                            <th class="tb-remove">Thao tác</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cart as $item)
                                        <form method="POST" action="{{route('update',['id'=>$item['id']])}}">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <tr>
                                            <td class="tb-image"><img src="{{asset('backend/image/' . $item['image'])}}" alt="" width="100px"></td>
                                            <td class="tb-product">
                                                <div class="product-name"><a href="#">{{$item['name']}}</a></div>
                                            </td>
                                            <td class="tb-price" style="width: 130px;">
                                                <span class="price">{{number_format($item['price'])}}</span>
                                            </td>
                                            <td class="tb-price">
                                                <span class="price">{{number_format($item['sale_price'])}}</span>
                                            </td>
                                          
                                            <td class="tb-qty">
                                                <div class="quantity">
                                                    <div class="buttons-added">
                                                        <input type="text" id="{{$item['id']}}" class="qty" value="{{$item['quantity']}}"  class=" input-text qty text" size="1" name="quantity">
                                                        <span style="margin-right: 10px"><i class="fa fa-plus" onclick="return tang({{$item['id']}})"></i></span>
                                                        <span class="" ><i class="fa fa-minus" onclick="return giam({{$item['id']}})"></i></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="tb-total">
                                                <span class="price">
                                                 <?php 
                                                    if($item['sale_price']>0) 
                                                             echo number_format($item['sale_price']*$item['quantity']);
                                                        else
                                                            echo number_format($item['price']*$item['quantity']);
                                                 ?> 
                                                 </span>
                                            </td>
                                            <td class="tb-remove">
                                                <!-- <span  class="update"><a href="#" class="btn btn-success" style="color: white" onclick="return update({{$item['id']}})" type="submit">Sửa</a></span> -->
                                                <button type="submit" class="update_buttom">Sửa</button> 
                                              	<span class=""><a href="{{route('deleteItemCart',['id'=>$item['id']])}}" class="btn btn-danger" style="color: white">Xóa</a></span>
                                            </td>
                                        </tr>
                                        </form>
                               			@endforeach
                               		   
                                    </tbody>
                                </table>
                            </div>
                            <div class="cart-actions">
                                <button type="submit" class="btn-continue">
                                    <span><a href="{{route('getIndexDiscover')}}">Tiếp tục mua</a></span>
                                </button>
                          		
                                <button type="submit" class="btn-update" >
                                    <span><a href="{{route('deleteCart')}}">Xóa giỏ hàng</a></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-xl-5 mt-5">
                        <div class="order-summary">
                            <h4 class="title-shopping-cart">đặt hàng</h4>
                            <div class="checkout-element-content">
                                <?php 
                                    $total = 0;
                                    foreach ($cart as $value) {
                                        if($value['sale_price']>0)
                                             $total += $value['quantity']*$value['sale_price'];
                                            else
                                             $total += $value['quantity']*$value['price'];
                                            }
                                            session(['total'=>$total]);
                                     ?>
                                <span class="order-left">Số tiền:<span>{{number_format($total)}}</span></span>
                                <span class="order-left">phí chuyển hàng:<span>miễn phí</span></span>
                                <span class="order-left">Tổng tiền:<span>{{number_format(session('total'))}}</span></span>
                                <ul>
                                    <li><label class="inline" ><input type="checkbox"><span class="input"></span>Tôi có mã khuyến mãi</label></li>
                                </ul>
                                <button type="submit" class="btn-checkout" >
                                    <span><a href="{{route('getCheckout')}}">Đặt hàng</a></span>
                                </button>
                            </div>
                        </div>
                    </div>
                        <?php } else {
                    	 ?>
                        <div class="alert alert-info">
                        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        	<strong>Không có mặt hàng nào</strong> <a href="{{route('home')}}">Mua hàng</a>
                        </div>
                    <?php } ?>
                    </div>
                    
                </div>
            </div>
</main>
        <script>
            function tang(a) {
                var id = "#" + a;
                var a = $(id).val();
                const b = new Number(a);
                var x = b+1;
                 $(id).attr('value',x);
            }
             function giam(a) {
                var id = "#" + a;
                var a = $(id).val();
                const b = new Number(a);
                var x = b-1;
                if (x<1) {
                   $(id).attr('value',1); 
                 }
                 else{
                 $(id).attr('value',x);
             }
            }
        //     function update(a) {
        //         var id = "#" + a;
        //         var qty = $(id).val();
            
        // $.get(
        //     'http://dung.com:8888/update/'+a+'/'+qty,
        //     function(data){
        //             alert('update thanh cong');
        //              }
        //              );
        //     }
        </script>
@stop()
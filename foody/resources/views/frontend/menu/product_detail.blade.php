@extends('frontend.master.master')
<!-- END nav -->
@section('title','Chi Tiết')
@section('header','Chi Tiết Món Ăn')
    
@section('content')

    <!-- END nav -->
    <section class="" style="background-color: #e6e6e6;padding-top: 20px;">
        <div class="container">
                  <div class="row" style="">
            <div class="col-md-5" style="overflow: hidden;">
                <img src="{{asset('backend/image/' . $pro->product_img)}}" alt="" width="100%" height="300px">
            </div>
            <div class="col-md-7">
                <h2>{{$pro->product_name}}</h2>
                <div>
                   <h4>Giá bán:
                    @if($pro->sale_off>0)
                    <del>{{number_format($pro->price)}}</del>
                    {{number_format($pro->price*$pro->sale_off/100)}} VNĐ
                    @else
                    {{number_format($pro->price)}} VNĐ
                    @endif
                   </h4>  
                </div>
                <div>
                    <span><h4>Số lượng:</h4></span>
                    <span><input type="number" value="1" min="1" size="10" id="qty"></span>
                </div>
                <div onclick="return AddItem({{ $pro->id }})" class="btn btn-danger" style="margin-top: 10px;">Mua ngay</div>
                <input type="text" id="ids" hidden value="{{$pro->id}}">
            </div>
        </div>
    <div class="row">
  <div class="col-2">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Món ăn khác</a>
      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Bình luận</a>
    </div>
  </div>
  <div class="col-10">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
          <section style="background-color: white">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="padding-left: 15px;">
                            <h3>Những món ăn khác</h3>
                            </div>
                            <div class="panel-body">
                                <div class="container product">
                                    <div class="row">
                                        @foreach($product as $pros)
                                        <div class="col-md-6">
                                            <div>
                                                <div style="overflow: hidden;float: left;">
                                                    <div class="media">
                                                        <a class="pull-left" href="#">
                                                            <img class="media-object" src="{{asset('backend/image/' . $pros->product_img)}}" alt="Image" width="60px">
                                                        </a>
                                                        <div class="media-body" style="padding-left: 10px">
                                                            <h4 class="media-heading" style="margin-bottom: 0">{{$pros->product_name}}</h4>
                                                            <?php 
                                                                if ($pros->sld>0) { ?>
                                                                    <p>Đã được đặt </p>
                                                             <?php   }
                                                             ?>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="float: right;text-align: right;">
                                                    <span>
                                                    <?php
                                                        if ($pros->sale_off>0) { ?>
                                                             <del>{{number_format($pros->price)}}</del>&nbsp;
                                                             {{number_format($pros->price*$pros->sale_off/100)}} &nbsp;
                                                         <?php } else {?>
                                                            {{number_format($pros->price)}}
                                                    <?php } ?>
                                                     <i class="fa fa-plus-square" aria-hidden="true" style="color: red"
                                                       onclick="return Add({{$pros->id}})" ></i>
                                                     <input type="text" hidden id="{{$pros->id}}" value="{{$pros->id}}">
                                            </span>
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </section>
      </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
          <section style="margin-top: 15px;background-color: #eee">
                @foreach ($post as $item)

                        <div style="background-color: white;padding: 10px;margin-bottom: 10px;">
                            <div class="media" style="padding: 10px;">
                                <div class="media-body">
                                <h5 class="media-heading" style="margin-bottom: 0">{{ $item->customer->name }}</h5>
                                <p>{{$item->created_at}}</p>
                                </div>
                            </div>
                            <hr>
                            <p>
                                {{ $item->conntent }}
                            </p>
                            
                        </div>
                                        
            @endforeach
            <div>
                <form action="" id="form-bl" method="post" class="bomanh">
                    @csrf
                    <input name="comment" type="text" placeholder="Viết bình luận...">
                    <button class="btn-sb" type="submit">Bình Luận</button>
                </form>
                </div>
                    </section>
      </div>
     
    </div>
  </div>
 </div>
    </section>
@stop()
<script type="text/javascript">
  function AddItem(id) {
        var qty = $("#qty").val();
        $.get(
            'http://myfoody.local.com:3000/addCart/'+id+'/'+qty,
            function(data){
                    alert('Mua hàng thành công');
                     }
        );
  }
  function Add(id) {
        var qty = 1;
        $.get(
            'http://myfoody.local.com:3000/addCart/'+id+'/'+qty,
            function(data){
                    alert('Mua hàng thành công');
                     }
        );
  }
</script>


@extends('frontend.master.master')
<!-- END nav -->
@section('title','Thực Đơn')
    
@section('header','Thực Đơn')


@section('content')
<section class="hero-wrap hero-wrap-2 mt-4" style="background-image: url('{{asset('frontend/images/search.jpg')}}');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">Tìm kiếm gần tôi</h1>
                <p class="breadcrumbs"><span class="mr-2" style="border-bottom: 0">
                <form action="{{ route('search.prd')}}" method="get" class="bomanh">
                <input name="search" type="search" placeholder="Nhập địa chỉ của bạn" size="100%" class="box_search"></span>
                <button class="buttom_search">Tìm kiếm</button></p>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="food">
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <img src="{{asset('frontend/images/images.jpg')}}" alt="" style="width: 40px">
                    <span style="font-size: 30px;color:red;">Thực Đơn</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Nổi bật</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Ưu đãi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Đặt nhiều nhất</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="product">
                        <div class="container">
                            <div class="row">
                                
                                @foreach($food_endow as $pro)
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                                    <div class="thumbnail">
                                        <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><img src="{{asset('backend/image/' . $pro->product_img)}}" alt="" width="100%" height="160px"></a>
                                        <div class="caption">
                                            <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><h3>{{$pro->product_name}}</h3></a>
                                            <p>
                                                {{Str::limit($pro->address,28)}}
                                            </p>
                                            <hr>
                                            <p>Giảm giá {{$pro->sale_off}} %</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div style="margin:auto">
                                    {{ $food_endow->links() }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="product">
                        <div class="container">
                            <div class="row">
                                @foreach($food_sale as $pro)
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                                    <div class="thumbnail">
                                        <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><img src="{{asset('backend/image/' . $pro->product_img)}}" alt="" width="100%" height="150px"></a>
                                        <div class="caption">
                                            <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><h3>{{$pro->product_name}}</h3></a>
                                            <p>
                                                {{Str::limit($pro->address,28)}}
                                            </p>
                                            <hr>
                                            <p>Giảm giá {{$pro->sale_off}} %</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div style="margin:auto">
                                    {{ $food_sale->links() }}
                                </div>                              
                            </div>
                            
                        </div>                        
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="product">
                        <div class="container">
                            <div class="row">
                                @foreach($food_sld as $pro)
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                                    <div class="thumbnail">
                                        <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><img src="{{asset('backend/image/' . $pro->product_img)}}" alt="" width="100%" height="150px"></a>
                                        <div class="caption">
                                            <a href="{{route('getDetail',['slug'=>$pro->prd_slug])}}"><h3>{{$pro->product_name}}</h3></a>
                                            <p>
                                                {{Str::limit($pro->address,28)}}
                                            </p>
                                            <hr>
                                            <p>Giảm giá {{$pro->sale_off}} %</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach    
                                <div style="margin:auto">
                                    {{ $food_sld->links() }}
                                </div>                          
                            </div>
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="col-md-8" style="margin:auto;">
    <hr style="border-top: 1px solid #939393">
</div>


@stop()

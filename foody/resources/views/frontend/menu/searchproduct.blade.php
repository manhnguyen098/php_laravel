@extends('frontend.master.master')
@section('content')
<section class="food">
    <p style="text-align:center;">Kết Quả Tìm Kiếm</p>
<div class="product">
    <div class="container">
        <div class="row">
            @if (session()->has('search_prd'))
                <div class="alert alert-danger" role="alert" style="margin:auto">
                    {{ session('search_prd') }}
                </div>        
            @endif
            @foreach($prd as $item)
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                <div class="thumbnail">
                    <a href=""><img src="{{asset('backend/image/' . $item->product_img)}}" alt="" width="100%"></a>
                    <div class="caption">
                        <a href=""><h3>{{$item->product_name}}</h3></a>
                        <p>
                            {{Str::limit($item->address,25)}}
                        </p>
                        <hr>
                    <p>Khoảng cách: {{ round($item->km,2)}} km</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
</section>
@endsection
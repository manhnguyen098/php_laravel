<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <!-- <a class="navbar-brand" href="index.html">ĐM</a> -->
        <img src="{{asset('frontend/images/logo.png')}}" alt="">
                <div class="dropdown" style="margin-left: 5px">

                    <div class="dropdown">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                          @if (session()->has('local'))
                              {{ session('local') }}
                          @else
                              Hà Nội
                          @endif
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('local',['local'=>'Hà Nội'])}}">Hà Nội</a>
                            <a class="dropdown-item" href="{{ route('local',['local'=>'Hồ Chí Minh'])}}">Hồ Chí Minh</a>
                        </div>
                      </div>  
    
</div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse menu" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item "><a href="{{route('index')}}" class="nav-link">Trang Chủ</a></li>
                <li class="nav-item"><a href="{{route('getIndexDiscover')}}" class="nav-link">Thực Đơn</a></li>
                <li class="nav-item"><a href="{{route('get.allrestaurants')}}" class="nav-link">Nhà Hàng</a></li>
            <li class="nav-item"><a href="{{ route('signIn')}}" class="nav-link">Đăng Kí Bán Hàng</a></li>
                <li class="nav-item"><a href="{{route('getCart')}}" class="nav-link">Giỏ Hàng</a></li>
               <!--  <li style="padding-top: 20px">
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0;background-color: white">
                            <img src="images/vn.png" alt="" width="20px">
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="margin-top: 0;padding: 0;top:29px;min-width:60px;border-radius:0">
                            <img src="images/us.png" alt="" width="20px">
                            <span>EN</span>
                        </div>
                    </div>
                </li> -->

            </ul>
        </div>
    </div>
</nav>
<section class="hero-wrap hero-wrap-2" style="background-image: url('{{asset('frontend/images/bg_4.jpg')}}');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">@yield('header')</h1>
                
            </div>
        </div>
    </div>
</section>
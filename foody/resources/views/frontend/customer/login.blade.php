<!DOCTYPE html>
<html lang="en">
<head>
	<title>Đăng nhập</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('frontend/images/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/select2.min.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/daterangepicker.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		
		<div class="container-login100" style="background-image: url('{{asset('frontend/images/bg-01.jpg')}}');">
			
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				@if(session('thongbao'))
					<div class="alert alert-primary" role="alert">
						{{session('thongbao')}}
				  	</div>
					@endif
				<form class="login100-form validate-form" method="post">
					@csrf
					<span class="login100-form-title p-b-49">
						Login
					</span>
				
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
						<span class="label-input100">Email</span>
						<input class="input100" type="email" name="email">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
						
					</div>
					{!! showError($errors,'email') !!}
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Mật khẩu</span>
						<input class="input100" type="password" name="password" >
						<span class="focus-input100" data-symbol="&#xf190;"></span>
						
					</div>
					{!! showError($errors,'password') !!}
					<div class="text-right p-t-8 p-b-31">
						<a href="{{ route('getRegister')}}">Đăng kí</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Đăng nhập
							</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/popper.js')}}"></script>
	<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/moment.min.js')}}"></script>
	<script src="{{asset('frontend/js/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('frontend/js/main.js')}}"></script>

</body>
</html>
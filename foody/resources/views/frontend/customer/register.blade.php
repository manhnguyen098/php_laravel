<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng kí</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('frontend/fonts/iconic/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('frontend/css/style1.css')}}">
</head>
<body>

    <div class="main" style="padding: 0">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content" style="padding: 50px">
                    <div class="signup-form">
                        <h2 class="form-title">Sign up</h2>
                        @if(session('thongbao'))
					<div class="alert alert-primary" role="alert">
						{{session('thongbao')}}
				  	</div>
					@endif
                        <form method="POST" class="register-form" id="register-form">
                            @csrf
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="name" id="name" placeholder="Your Name"/>
                            </div>
                            {!! showError($errors,'name') !!}
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            {!! showError($errors,'email') !!}
                            <div class="form-group">
                                <label for="address"><i class="zmdi zmdi-pin" aria-hidden="true"></i>
                                </label>
                                <input type="text" name="address" id="address" placeholder="Your Address"/>
                            </div>
                            {!! showError($errors,'address') !!}
                            <div class="form-group">
                                <label for="phone"><i class="zmdi zmdi-phones" aria-hidden="true"></i>
                                </label>
                                <input type="text" name="phone" id="phone" placeholder="Your Phone"/>
                            </div>
                            {!! showError($errors,'phone') !!}
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="pass" placeholder="Password"/>
                            </div>
                            {!! showError($errors,'password') !!}
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="re_pass" id="re_pass" placeholder="Repeat your password"/>
                            </div>
                           
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{asset('frontend/images/breakfast-4.jpg')}}" alt="sing up image"></figure>
                        <a href="#" class="signup-image-link">I am already member</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sing in  Form -->
    </div>

    <!-- JS -->
    <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/js/main1.js')}}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
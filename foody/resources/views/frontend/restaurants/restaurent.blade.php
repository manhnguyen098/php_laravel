@extends('frontend.master.master')
<!-- END nav -->
@section('title','Nhà Hàng')
    
@section('header','Nhà Hàng')
    
@section('content')
    <!-- END nav -->
    <section class="hero-wrap hero-wrap-2 mt-4" style="background-image: url('{{asset('frontend/images/search.jpg')}}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">Tìm kiếm nhà hàng</h1>
                    <p class="breadcrumbs"><span class="mr-2" style="border-bottom: 0">
                    <form action="{{ route('search.res')}}" method="get" class="bomanh">
                    <input name="search" type="search" placeholder="Tìm kiếm nhà hàng" size="100%" class="box_search"></span>
                    <button class="buttom_search">Tìm kiếm</button></p>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="food">
        <div class="container">
            <div class="row">
                <div class="menu1 container">
                    <ul class="" style="float: left;overflow: hidden;height: 50px;">
                        <li class="icon" style="line-height: 50px;height: 50px;padding-left: 0;margin-left: 0">
                            <img src="{{asset('frontend/images/icon-hot.png')}}" alt="" width="55px">
                            <span style="font-size: 30px;color:red;margin-left: 15px;">Danh Sách Nhà Hàng</span>
                        </li>
                    </ul>
  
                </div>
                <div class="product">
                    <div class="container">
                        <div class="row">
                            @foreach($res as $item)
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                                <div class="thumbnail">
                                    <a href="{{ route('res.detail',['slug'=>$item->res_slug]) }}"><img src="{{asset('backend/image/' . $item->image)}}" alt="" width="100%" height="150px"></a>
                                    <div class="caption">
                                        <a href="{{ route('res.detail',['slug'=>$item->res_slug]) }}"><h3>{{Str::limit($item->res_name,15)}}</h3></a>
                                        <p>
                                            {{Str::limit($item->address,25)}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>


@stop()
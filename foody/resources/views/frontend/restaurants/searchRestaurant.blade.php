@extends('frontend.master.master')
@section('content')
<section class="food">
    <p style="text-align:center;">Kết Quả Tìm Kiếm</p>
<div class="product">
    <div class="container">
        <div class="row">
            @if (session()->has('search_res'))
            
            <div class="alert alert-danger" role="alert" style="margin:auto">
                {{ session('search_res') }}
            </div>
                
            @endif
            @foreach($res as $item)
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item">
                <div class="thumbnail">
                    <a href="{{ route('res.detail',['slug'=>$item->res_slug]) }}"><img src="{{asset('backend/image/' . $item->image)}}" alt="" width="100%" height="180px"></a>
                    <div class="caption">
                        <a href="{{ route('res.detail',['slug'=>$item->res_slug]) }}"><h3>{{Str::limit($item->res_name,15)}}</h3></a>
                        <p>
                            {{Str::limit($item->address,25)}}
                        </p>
                        <hr>
                    <p>Khoảng cách: {{ round($item->km,2)}} km</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
</section>
@endsection
@extends('frontend.master.master')
<!-- END nav -->
@section('title','Tin Tức')
@section('header','Tin Tức')
    
@section('content')
<!-- END nav -->
<section class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-md-5" style="overflow: hidden;">
            <img src="{{asset('backend/image/' . $detail->new_image)}}" alt="" width="100%" height="300px">
        </div>
        <div class="col-md-7">
            <h2>{{ $detail->title }}</h2>
            <p>
                {{ $detail->content}}
            </p>
        </div>
    </div>

</section>
<section class="" style="background-color: #e6e6e6;padding-top: 20px;">
    <div class="container">

        <div class="row">
            <div class="card">
                <div class="card-header">
                    Tin tức khác
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($post as $item)
                            

                        <div class="col-md-6">
                            <div class="card" style="width: 100%;">
                                <img src="{{asset('backend/image/' . $item->new_image)}}" class="card-img-top" alt="..." height="400px" width="100%">
                                <div class="card-body">
                                <h5 class="card-title">{{$item->title}}</h5>
                                <p class="card-text">{{ Str::limit($item->content,20)}}</p>
                                    <a href="{{ route('detail.post',['slug'=>$item->title_slug])}}" class="btn btn-primary">Xem</a>
                                </div>
                            </div>
                        </div>

                        @endforeach
                        <div style="margin:auto">
                            {{ $post->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

</section>
@stop()
@extends('frontend.master.master')
<!-- END nav -->
@section('title','Chi Tiết')
@section('header','Chi Tiết Nhà Hàng')
@section('content')
<!-- END nav -->
<section class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-md-5" style="overflow: hidden;">
            <img src="{{asset('backend/image/' . $detail->image)}}" alt="" width="100%" height="300px">
        </div>
        <div class="col-md-7">
            <h2>{{ $detail->res_name }}</h2>
            <p><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ $detail->address }}
            </p>
            <p>
                <i class="fa fa-clock-o" aria-hidden="true"></i> Giờ mở cửa {{ $detail->time_on  }} {{ $detail->time_off }}
            </p>
            <p><i class="fa fa-commenting-o" aria-hidden="true"></i> {{ $total }} bình luận
            </p>
        <span class="btn btn-primary">Liên Hệ Đặt Bàn {{ $detail->phone}}</span>
        </div>
    </div>

</section>
<section class="" style="background-color: #e6e6e6;padding-top: 20px;">
    <div class="container">

        <div class="row">
            <div class="col-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Thực đơn</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Bình luận</a>

                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Tin tức</a>
                </div>
            </div>
            <div class="col-10">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <section style="background-color: white">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="padding-left: 15px;">
                                    <h3>Món Ăn</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="container product">
                                        <div class="row">
                                            @foreach ($prd as $item)
                                            <div class="col-md-6">
                                                <div>
                                                    <div style="overflow: hidden;float: left;">
                                                        <div class="media">
                                                            <a class="pull-left" href="{{route('getDetail',['slug'=>$item->prd_slug])}}">
                                                                <img class="media-object" src="{{asset('backend/image/' . $item->product_img)}}" alt="Image" width="60px">
                                                            </a>
                                                            <div class="media-body" style="padding-left: 10px">
                                                                <h4 class="media-heading" style="margin-bottom: 0"></h4>
                                                                <a href="{{route('getDetail',['slug'=>$item->prd_slug])}}"><p>{{ $item->product_name}}</p></a>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="float: right;text-align: right;">
                                                        <span>

                                                            &nbsp;
                                                             {{ number_format($item->price) }} VND &nbsp;

                                                     <i class="fa fa-plus-square" aria-hidden="true" style="color: red"
                                                       onclick="return Add({{ $item->id }})"></i>
                                                     <input type="text" hidden id="" value="">
                                                    </span>
                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <section style="margin-top: 15px;background-color: #eee">
                           @foreach ($comments as $item)
                            <div style="background-color: white;padding: 10px;margin-bottom: 10px;">
                                <div class="media" style="padding: 10px;">
                                    <div class="media-body">
                                    <h5 class="media-heading" style="margin-bottom: 0">{{ $item->customer->name }}</h5>
                                    <p>{{ $item->created_at}}</p>
                                    </div>
                                </div>
                                <hr>
                                <p>
                                    {{ $item->conntent }}
                                </p>
                            </div>
                            @endforeach
                       
                            <div>
                            <form action="" id="form-bl" method="post" class="bomanh">
                                @csrf
                                <input name="comment" type="text" placeholder="Viết bình luận...">
                                <button class="btn-sb" type="submit">Bình Luận</button>
                            </form>
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">Khuyến mãi</div>
                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                        <div class="card">
                            <div class="card-header">
                                Tin tức
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    @foreach ($post as $item)
                                        

                                    <div class="col-md-6">
                                        <div class="card" style="width: 100%;">
                                            <img src="{{asset('backend/image/' . $item->new_image)}}" class="card-img-top" alt="...">
                                            <div class="card-body">
                                            <h5 class="card-title">{{$item->title}}</h5>
                                            <p class="card-text">{{ Str::limit($item->content,20)}}</p>
                                            <a href="{{ route('detail.post',['slug'=>$item->title_slug])}}" class="btn btn-primary">Xem</a>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
    function Add(id) {
        var qty = 1;
        $.get(
            'http://myfoody.local.com:3000/addCart/'+id+'/'+qty,
            function(data){
                    alert('Mua hàng thành công');
                     }
        );
  }
  </script>
@stop()
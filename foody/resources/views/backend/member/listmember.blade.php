@extends('backend.master.master')
@section('title','Danh sách member')
@section('tv')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Danh sách</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      Họ
                    </th>
                    <th>
                      Tên
                    </th>
                    <th>
                      Email
                    </th>
                    <th>
                      Số điện thoại
                    </th>
                    <th>
                      Địa chỉ
                    </th>
                    <th style="text-align: center">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($member as $row)
                    <tr>
                      <td>
                        {{ $row->fist_name }}
                      </td>
                      <td>
                        {{ $row->last_name }}
                      </td>
                      <td>
                        {{ $row->email }}
                      </td>
                     
                      <td>
                        {{ $row->phone }}
                      </td>
                      <td>
                        {{ $row->address }}
                      </td>
                      <td>
                      <a onclick="return del('{{ $row->last_name}}')" href="{{ route('del.member',['id'=>$row->id])}}" class="btn btn-primary pull-right">Xóa</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div>
        {{ $member->links() }}
    </div>
  </div>
  <script>
    function del(name){
      return confirm('Bạn có muốn xóa sản phẩm: '+name);
    }
  </script>
@endsection
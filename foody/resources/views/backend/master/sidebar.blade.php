<div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text logo-normal">
      Quản lý
    </a>
  </div>
<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item  @yield('home')">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
          <p>Trang chủ</p>
        </a>
      </li>
      <li class="nav-item  @yield('user')">
        <a class="nav-link" href="{{ route('get.user',['id'=>Auth::user()->id]) }}">
          <i class="material-icons">person</i>
          <p>Tài Khoản</p>
        </a>
      </li>
      @if (Auth::user()->level == 1)
      <li class="nav-item @yield('tv')">
        <a class="nav-link" href="{{ route('get.member') }}">
          <i class="material-icons">content_paste</i>
          <p>Quản lý thành viên</p>
        </a>
      </li>
      @endif
      <li class="nav-item @yield('res')">
        <a class="nav-link" href="{{ route('list.res') }}">
          <i class="material-icons">content_paste</i>
          <p>Danh sách nhà hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('addres')">
        <a class="nav-link" href="{{ route('add.res') }}">
          <i class="fa fa-plus"></i>
          <p>Thêm nhà hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('product')">
        <a class="nav-link" href="{{ route('list.product') }}">
          <i class="material-icons">content_paste</i>
          <p>Danh sách sản phẩm</p>
        </a>
      </li>
      <li class="nav-item @yield('addproduct')">
        <a class="nav-link" href="{{ route('add.product') }}">
          <i class="fa fa-plus"></i>
          <p>Thêm sản phẩm</p>
        </a>
      </li>
      <li class="nav-item @yield('order')">
        <a class="nav-link" href="{{ route('get.order') }}">
          <i class="material-icons">content_paste</i>
          <p>Danh sách đặt hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('posts')">
        <a class="nav-link" href="{{ route('get.tin') }}">
          <i class="material-icons">content_paste</i>
          <p>Tin Tức</p>
        </a>
      </li>
      <li class="nav-item @yield('addposts')">
        <a class="nav-link" href="{{ route('add.tin') }}">
          <i class="fa fa-plus"></i>
          <p>Thêm Tin tức</p>
        </a>
      </li>
      <li class="nav-item @yield('comment')">
        <a class="nav-link" href="{{ route('get.comment') }}">
          <i class="material-icons">content_paste</i>
          <p>Bình luận nhà hàng</p>
        </a>
      </li>
      <li class="nav-item @yield('commentprd')">
        <a class="nav-link" href="{{ route('get.commentprd') }}">
          <i class="material-icons">content_paste</i>
          <p>Bình luận sản phẩm</p>
        </a>
      </li>
      

      
    </ul>
  </div>
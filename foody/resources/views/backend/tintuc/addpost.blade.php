@extends('backend.master.master')
@section('title','Thêm Tin tức')
@section('addposts')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Thêm Tin Tức</h4>
             
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Tiêu Đề</label>
                      <input name="title" type="text" class="form-control">
                    </div>
                    {!! showError($errors,'title') !!}
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">                    
                    <label>Nhà hàng</label>
                      <select class="form-control" name="restaurant">                              
                          @foreach ($res as $row)
                          <option value="{{ $row->id }}" selected="">{{ $row->res_name }}</option>
                          @endforeach                             
                      </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Nội Dung</label>
                          <div class="form-group">
                            <textarea name="content" class="form-control" rows="5"></textarea>
                          </div>
                        </div>
                      </div>
                      {!! showError($errors,'content') !!}
                </div>
                <div class="row">
                <div class="col-md-3">
                    <input name="image" id="image" type="file" class="form-control">   
                    {!! showError($errors,'image') !!}             
                </div>
                
            </div>
            
                <button type="submit" class="btn btn-primary pull-right">Thêm Tin Tức</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
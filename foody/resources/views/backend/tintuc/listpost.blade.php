@extends('backend.master.master')
@section('title','Tin Tuc')
@section('posts')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tin Tức</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      Tiêu Đề
                    </th>
                    <th>
                      Nội Dung
                    </th>
                    <th>
                      Nhà Hàng
                    </th>
                    <th>
                      Ảnh
                    </th>
                    <th style="text-align: center">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($postres as $row)

                    <tr>
                      <td>
                        {{ $row->title }}
                      </td>
                      <td>
                        {{ $row->content }}
                      </td>
                      <td class="text-primary">
                        {{ $row->res_name }}
                      </td>
                      <td class="text-primary">
                        <div class="img-xx"><img src="{{ asset('backend/image/' . $row->new_image) }}" alt="">
                        </div>
                      </td>
                      <td>
                        <a href="{{ route('edit.tin',['id'=> $row->id]) }}" class="btn btn-primary pull-right">Sửa</a>
                        <a onclick="return del()" href="{{ route('del.tin',['id'=> $row->id]) }}" class="btn btn-primary pull-right">Xóa</a>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div style="margin:auto">
            {{ $postres->links() }}
        </div>
      </div>
    </div>
  </div>
  <script>
    function del(){
      return confirm('Bạn có muốn xóa tin tức');
    }
  </script>
@endsection
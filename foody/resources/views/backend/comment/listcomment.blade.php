@extends('backend.master.master')
@section('title','Comment ')
@section('comment')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Bình luận</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                    Nội dung
                    </th>
                    <th>
                    Nhà Hàng
                    </th>
                    <th style="text-align: center">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($comments as $row)
                    <tr>
                      <td>
                        {{ $row->conntent}}
                      </td>
                      <td>
                        {{ $row->res_name }}
                      </td>
                      <td>
                      <a onclick="return del()" href="{{ route('del.comment',['id'=>$row->id])}}" class="btn btn-primary pull-right">Del</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div style="margin:auto"> 
        {{ $comments->links() }}
    </div>
  </div>
  <script>
    function del(){
      return confirm('Bạn có muốn xóa tin tức');
    }
  </script>
@endsection
@extends('backend.master.master')
@section('title','Danh đặt hàng')
@section('order')
    active
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Danh sách đặt hàng</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                    Tên
                    </th>
                    <th>
                    Địa chỉ
                    </th>
                    <th>
                    Số điện thoại
                    </th>
                    <th>
                    Tên sản phẩm
                    </th>
                    <th>
                    Số lượng
                    </th>
                    <th>
                        Ảnh sản phẩm
                    </th>
                    <th>
                        Nhà Hàng
                    </th>

                  </thead>
                  <tbody>
                    @foreach ($orders as $row)
                    <tr>
                      <td>
                        {{ $row->name }}
                      </td>
                      <td>
                        {{ $row->address }}
                      </td>
                      <td>
                        {{ $row->phone }}
                      </td>
                     
                      <td>
                        {{ $row->product_name }}
                      </td>
                      <td>
                        {{ $row->quantity }}
                      </td>
                      <td class="text-primary">
                        <div class="img-xx"><img src="{{ asset('backend/image/' . $row->product_img) }}" alt="">
                        </div>
                      </td>
                      <td>
                        {{ $row->res_name }}
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div>
            {{ $orders->links() }}
        </div>
      </div>
    </div>
  </div>

@endsection
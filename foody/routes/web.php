<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


///trang quan tri
Route::get('login','admin\LoginController@getLogin')->name('login')->middleware('CheckLogout');
Route::post('login','admin\LoginController@postLogin');


Route::group(['prefix' => 'admin','namespace'=>'admin','middleware'=>'CheckLogin'], function () {
    Route::get('/','HomeController@index')->name('home');
    Route::get('logout','LoginController@logOut')->name('logout');
    Route::get('/user/detail/{id}','UserController@userDetail')->name('get.user');
    Route::post('/user/detail/{id}','UserController@editUser');
    //member
    Route::group(['prefix' => 'member','middleware'=>'CheckLevel'], function () {
        Route::get('/list','AdminController@getMember')->name('get.member');
    Route::get('/list/del/{id}','AdminController@delMember')->name('del.member');
    });
    ///nha hang
    Route::group(['prefix' => 'restaurant'], function () {
        Route::get('/','RestaurantController@list')->name('list.res');

        Route::get('/add','RestaurantController@addRestaurant')->name('add.res');
        Route::post('/add','RestaurantController@postAddRestaurant');

        Route::get('/edit/{id}','RestaurantController@editRestaurant')->name('edit.res');
        Route::post('/edit/{id}','RestaurantController@postEditRestaurant');

        Route::get('/del/{id}','RestaurantController@delRestaurant')->name('del.res');

        Route::get('/comment/list','CommentController@getAllComment')->name('get.comment');
        Route::get('/comment/list/del/{id}','CommentController@delComment')->name('del.comment');

        
    });

    ///san ppham
    Route::group(['prefix' => 'product'], function () {
        Route::get('/list','ProductController@listProduct')->name('list.product');
        Route::get('/add','ProductController@addProduct')->name('add.product');
        Route::post('/add','ProductController@postAddProduct');
        Route::get('list/edit/{id}','ProductController@editProduct')->name('edit.product');
        Route::post('list/edit/{id}','ProductController@postEditProduct');
        Route::get('list/del/{id}','ProductController@delProduct')->name('del.product');
        Route::get('/comment/list','CommentController@getCommentProduct')->name('get.commentprd');
        Route::get('/comment/list/del/{id}','CommentController@delCommentProduct')->name('del.commentprd');
    });
    //tin tuc
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/list','PostController@getAllPost')->name('get.tin');
        Route::get('add','PostController@addPost')->name('add.tin');
        Route::post('add','PostController@add');
        Route::get('/list/edit/{id}','PostController@editPost')->name('edit.tin');
        Route::post('/list/edit/{id}','PostController@edit');
        Route::get('/list/del/{id}','PostController@delPost')->name('del.tin');
    });
    //order
    Route::get('/order','OrderController@getOrder')->name('get.order');
    
});


///giao dien nguoi dung
Route::group(['prefix' => 'menu/','namespace'=>'frontend'], function () {
        Route::get('','DiscoverController@index')->name('getIndexDiscover');
        Route::get('product','DiscoverController@searchProduct')->name('search.prd');
        Route::get('product_detail/{slug}.html','DiscoverController@getDetail')->name('getDetail');
        Route::post('product_detail/{slug}.html','DiscoverController@comment')->middleware('CheckComment');
});

    ///route nhà hàng
Route::group(['prefix' => 'restaurant','namespace'=>'frontend'], function () {
        Route::get('/','RestaurantController@getAll')->name('get.allrestaurants');
        Route::get('/detail/{slug}.html','RestaurantController@getDetail')->name('res.detail');
        Route::post('/detail/{slug}.html','RestaurantController@comment')->middleware('CheckComment');
        Route::get('/search','RestaurantController@searchRestaurant')->name('search.res');
        Route::get('/post/detail/{slug}.html','RestaurantController@detailPost')->name('detail.post');
});


Route::get('dang-nhap','frontend\CustomerController@getLogin')->name('get.login');
Route::post('dang-nhap','frontend\CustomerController@postLogin')->name('post.login');

Route::get('dang-ki','frontend\CustomerController@getRegister')->name('getRegister');
Route::post('dang-ki','frontend\CustomerController@postRegister')->name('postRegister');

/// route giỏ hàng
Route::get('cart','frontend\CartController@getCart')->name('getCart');
Route::get('addCart/{id}/{qty}','frontend\CartController@addCart')->name('addCart');
Route::get('deleteItemCart/{id}','frontend\CartController@deleteItemCart')->name('deleteItemCart');

//dang ki ban hang
Route::get('signin-sell','frontend\SigninsellController@signIn')->name('signIn');
Route::post('signin-sell','frontend\SigninsellController@postSignIn');

Route::post('update','frontend\CartController@update')->name('update');
Route::get('deleteCart','frontend\CartController@deleteCart')->name('deleteCart');
Route::get('getcheckout','frontend\CheckOutController@getCheckout')->name('getCheckout');
Route::post('checkout','frontend\CheckOutController@postCheckOut')->name('postCheckOut');

Route::get('/',function(){
    return view('frontend.index');
})->name('index');

Route::get('local/{local}','frontend\LocalController@local')->name('local');

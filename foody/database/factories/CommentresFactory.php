<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Commentres::class, function (Faker $faker) {
    return [
        'conntent'=>$faker->text($maxNbChars = 50),
        'restaurant_id'=>App\Model\Restaurant::all()->random()->id,
        'customer_id'=>App\Model\Customer::all()->random()->id,
    ];
});

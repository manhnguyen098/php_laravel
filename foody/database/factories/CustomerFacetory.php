<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Customer::class, function (Faker $faker) {
    return [
        'email'=>$faker->email,
        'password'=>bcrypt('123456'),
        'name'=>$faker->lastName,
        'phone'=>$faker->phoneNumber,
        'address'=>$faker->address
    ];
});

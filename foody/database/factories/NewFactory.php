<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Newres;
use Faker\Generator as Faker;

$factory->define(Newres::class, function (Faker $faker) {
    return [
        'title'=>$faker->text($maxNbChars = 10),
        'title_slug'=>$faker->slug,
        'new_image'=>'anh.jpg',
        'content'=>$faker->text($maxNbChars = 100),
        'restaurant_id'=>App\Model\Restaurant::all()->random()->id,
    ];
});

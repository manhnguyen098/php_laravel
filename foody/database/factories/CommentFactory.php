<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Comment::class, function (Faker $faker) {
    return [
        'conntent'=>$faker->text($maxNbChars = 50),
        'product_id'=>App\Model\Product::all()->random()->id,
        'customer_id'=>App\Model\Customer::all()->random()->id,
    ];
});

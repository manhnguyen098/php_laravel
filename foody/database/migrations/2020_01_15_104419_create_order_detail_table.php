<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders_detail');
        Schema::create('orders_detail', function (Blueprint $table) {
            $table->integer('order_id')->unsigned();
           $table->string('product_img');
           $table->string('product_name');
           $table->integer('restaurant_id')->unsigned();
           $table->integer('quantity');
           $table->integer('price');
           $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
           $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_detail');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Model\Comment;
class CommentresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Comment::class,100)->create();
    }
}

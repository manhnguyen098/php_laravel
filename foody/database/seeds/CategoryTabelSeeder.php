<?php

use Illuminate\Database\Seeder;

class CategoryTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->delete();
        DB::table('category')->insert([
            ['id'=>1,'name'=>'Đồ ăn','parent'=>0],
            ['id'=>2,'name'=>'Đồ uống','parent'=>0],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Model\Newres;
class NewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Newres::class,100)->create();
    }
}

<?php 

namespace App\helper;

/**
 * 
 */
class Cart 
{	
	public $id_restuanrent;
	public $items = [];
	public $total_quantity = 0;
	public $total_price = 0;
	function __construct()
	{
		$this->items = session('cart') ? session('cart') : [];
		$this->total_price = $this->getTotalPrice();

	}
	public function add($product,$quantity = 1)
	{
		$item =[
			'id' => $product->id,
			'name' =>$product->product_name,
			'image' =>$product->product_img,
			'price' =>$product->price,
			'sale_price' => $product->price*$product->sale_off/100,
			'quantity' => $quantity,
			'id_restuanrent'=>$product->restaurant_id
		];
		if(isset($this->items[$product->id])){
			$this->items[$product->id]['quantity'] += $quantity;	
		}else{
			$this->items[$product->id] = $item;
		}
		session(['cart'=>$this->items]);
	}
	public function update($id,$quantity)
	{
		if (isset($this->items[$id])) {
			$this->items[$id]['quantity'] = $quantity;
		}
		session(['cart'=>$this->items]);

	}
	public function deleteItem($id)
		{
			if (isset($this->items[$id])) {
				unset($this->items[$id]);
				session(['cart'=>$this->items]);
			
				// unset(session('cart')[$id]);
			}
		}	
	public function deleteCart()
	{
		session(['cart'=>'']);
	}
	private function getTotalQuantity(){

	}
	private function getTotalPrice(){
		$sum = 0;
		foreach ($this->items as  $value) {
			if($value['sale_price']>0)
			$sum += $value['quantity']*$value['sale_price'];
		else
			$sum += $value['quantity']*$value['price'];
		}
		return $sum;
	}
}
?>
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'title'=>'required',
             'content'=>'required',
             'image'=>'required',
         ];
     }
     public function messages()
     {
         return [
             'title.required'=>'Tiêu đề không được để trống!',
             'content.required'=>'Nội dung không được để trống!',
             'image.required'=>'Anh không được để trống!',
         ];
     }
}

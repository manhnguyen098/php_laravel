<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'city'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'time_on'=>'required',
            'time_off'=>'required',
            
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên nhà hàng không được để trống',
            'city.required'=>'Thành phố không được để trống',
            'time_on.required'=>'Giờ không được để trống',
            'time_off.required'=>'Giờ không được để trống',
            'address.required'=>'Địa chỉ không được để trống',
            'phone.required'=>'Số điện thoại không được để trống',
               
        ];
    }
}

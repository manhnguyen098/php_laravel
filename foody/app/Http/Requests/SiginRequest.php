<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
     {
         return [
             'email'=>'required|email|unique:users,email',
             'password'=>'required|min:5',
             'fist_name'=>'required',
             'last_name'=>'required',
             'address'=>'required',
             'phone'=>'required',
         ];
 
     }
    public function messages()
     {
         return [
             'email.required'=>'Email không được để trống',
             'email.unique'=>'Email không được trùng',
             'password.required'=>'Mật khẩu không được để trống',
             'password.min:5'=>'Mật khẩu không được dưới 5 kí tự',
             'fist_name.required'=>'Họ không được để trống',
             'last_name.required'=>'Tên không được để trống',
             'address.required'=>'Địa chỉ không được để trống',
             'phone.required'=>'Số điện thoại không được để trống',        
         ];
     }
}

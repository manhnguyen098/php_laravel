<?php

namespace App\Http\Controllers\admin;
use App\Http\Requests\RestaurantRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Restaurant;
use Illuminate\Support\Str;

class RestaurantController extends Controller
{
    public function list(){
        $data['res'] = Restaurant::where('user_id',Auth::user()->id)->get();
        return view('backend.restaurant.list',$data);
    }
    public function addRestaurant(){
        return view('backend.restaurant.add');
    }
    public function postAddRestaurant(RestaurantRequest $r){
        $address = $r->address;
        $apiKey = 'AIzaSyD7gzwc-HNYPk-Wz8IaiAIfInK0t9p5ISo'; 
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);
        $geo = json_decode($geo, true);
        ///get lat long google map api
        $lat = $geo['results'][0]['geometry']['location']['lat'];
        $lng = $geo['results'][0]['geometry']['location']['lng'];

        $res = new Restaurant;
        $res->res_name = $r->name;
        $res->res_slug = Str::slug($r->name,'-');
        $res->city = $r->city;
        $res->address = $r->address;
        $res->phone = $r->phone;
        $res->long = $lng;
        $res->lat = $lat;
        $res->time_on = $r->time_on;
        $res->time_off = $r->time_off;
        $res->user_id = Auth::user()->id;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $res->image = $filename;
        }
        $res->save();
        return redirect('admin/restaurant');
    }
    public function editRestaurant($id){
        $data['res'] = Restaurant::find($id);
        return view('backend.restaurant.edit',$data);
    }
    public function postEditRestaurant(RestaurantRequest $r,$id){
        $address = $r->address;
        $apiKey = 'AIzaSyD7gzwc-HNYPk-Wz8IaiAIfInK0t9p5ISo'; 
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);
        $geo = json_decode($geo, true);
        /// get lat long google map api
        $lat = $geo['results'][0]['geometry']['location']['lat'];
        $lng = $geo['results'][0]['geometry']['location']['lng'];

        $res = Restaurant::find($id);
        $res->res_name = $r->name;
        $res->res_slug = Str::slug($r->name,'-');
        $res->city = $r->city;
        $res->address = $r->address;
        $res->phone = $r->phone;
        $res->long = $lng;
        $res->lat = $lat;
        $res->time_on = $r->time_on;
        $res->time_off = $r->time_off;
        if($r->hasFile('image')){
            if($res->image !='anh1.jpg'){
                unlink('backend/image/'.$res->image);
            }
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $res->image = $filename;
        }
        
        $res->save();
        return redirect('admin/restaurant');
    }
    public function delRestaurant($id){
        Restaurant::destroy($id);
        return redirect('admin/restaurant');
    }

}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Model\{Commentres,Restaurant,Comment};


class CommentController extends Controller
{
    public function getAllComment(){
        $data['comments'] = DB::table('restaurants')->join('commentres','restaurants.id','=','commentres.restaurant_id')
        ->where('user_id',Auth::user()->id)->paginate(10);
        
        return view('backend.comment.listcomment',$data);
    }
    public function delComment($id){
        Commentres::destroy($id);

        return redirect()->back();
    }
    public function getCommentProduct(){
        $data['cmt'] = DB::table('restaurants')->join('products','restaurants.id','=','products.restaurant_id')
        ->join('comments','comments.product_id','=','products.id')
        ->where('user_id',Auth::user()->id)->paginate(10);

        return view('backend.comment.listcmproduct',$data);
    }
    public function delCommentProduct($id){
        Comment::destroy($id);
        return redirect()->back();
    }
}

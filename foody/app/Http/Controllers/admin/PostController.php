<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use App\Model\{Newres,Restaurant};

class PostController extends Controller
{
    public function getAllPost(){
        $data['postres'] = DB::table('restaurants')->join('news','news.restaurant_id','=','restaurants.id')
        ->where('user_id',Auth::user()->id)->paginate(10);

        return view('backend.tintuc.listpost',$data);
    }
    public function addPost(){
        $data['res'] = Restaurant::where('user_id','=',Auth::user()->id)->get();
        return view('backend.tintuc.addpost',$data);
    }
    public function add(PostsRequest $r)
    {
        $tt = new Newres;
        $tt->title = $r->title;
        $tt->title_slug = Str::slug($r->title,'-');
        $tt->content = $r->content;
        $tt->restaurant_id = $r->restaurant;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $tt->new_image = $filename;
        }
        $tt->save();
        return redirect('admin/posts/list');
    }
    public function editPost($id){
        $data['tt'] = Newres::find($id);
        $data['res'] = Restaurant::where('user_id','=',Auth::user()->id)->get()->toArray();
        return view('backend.tintuc.editpost',$data);
    }
    public function edit(PostsRequest $r,$id){
        $tt = Newres::find($id);
        $tt->title = $r->title;
        $tt->title_slug = Str::slug($r->title,'-');
        $tt->content = $r->content;
        $tt->restaurant_id = $r->restaurant;
        if($r->hasFile('image')){
            $image = $r->file('image');
            $filename = $image->getClientOriginalName();
            $image->move('backend/image',$filename);
            $tt->new_image = $filename;
        }
        $tt->save();
        return redirect('admin/posts/list');
    }
    public function delPost($id){
        Newres::destroy($id);
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function getMember(){
        $data['member'] = User::where('level',2)->paginate(10);
        return view('backend.member.listmember',$data);
    }
    public function delMember($id)
    {   
        User::destroy($id);
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Order,Orderdetail,Restaurant,Product};
use DB;
class OrderController extends Controller
{
    public function getOrder(){
        $data['orders'] = DB::table('orders')->join('orders_detail','orders.id','=','orders_detail.order_id')
        ->join('restaurants','restaurants.id','=','orders_detail.restaurant_id')->where('status',0)->where('user_id',Auth::user()->id)
        ->select('orders.phone','name','orders.address','quantity','orders_detail.product_name','orders_detail.product_img','res_name')->paginate(10);
        return view('backend.order.order',$data);
    }
    // public function getOrderDetail(){

    // }
}

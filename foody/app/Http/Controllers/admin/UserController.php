<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function userDetail($id){
        $data['user'] = User::find($id);
        return view('backend.user.user',$data);
    }
    public function editUser(Request $r,$id){
        $user = User::find($id);
        $user->email = $r->email;
        if($r->password!=""){
            $user->password = bcrypt($r->password);
        }
        $user->fist_name = $r->fist_name;
        $user->last_name = $r->last_name;
        $user->phone = $r->phone;
        $user->address = $r->address;
        $user->save();
        return redirect()->back();
    }
    
}

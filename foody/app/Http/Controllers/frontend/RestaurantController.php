<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Restaurant,Product,Newres,Commentres};
use DB;
class RestaurantController extends Controller
{
    public function getAll(){
        if(session()->has('local')){
            $local = session('local');
        }else {
            $local = 'Hà Nội';
        }

        $data['res'] = Restaurant::where('city',$local)->get();
        return view('frontend.restaurants.restaurent',$data);
    }
    public function getDetail($slug){
        $detail = Restaurant::where('res_slug',$slug)->first();
        $prd = Product::where('restaurant_id',$detail->id)->get();
        $comments = Commentres::where('restaurant_id',$detail->id)->get();
        $total = Commentres::where('restaurant_id',$detail->id)->count();
        $post = Newres::where('restaurant_id',$detail->id)->get();
        return view('frontend.restaurants.restaurantdetail',compact('detail','prd','comments','post','total'));
    }
    public function searchRestaurant(Request $r){
        $address = $r->search;
        $apiKey = 'AIzaSyD7gzwc-HNYPk-Wz8IaiAIfInK0t9p5ISo'; 
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);
        $geo = json_decode($geo, true);

        $lat = $geo['results'][0]['geometry']['location']['lat'];
        $lng = $geo['results'][0]['geometry']['location']['lng'];

        $data['res']=DB::table('restaurants')
        ->select('address','res_name','image','res_slug',DB::raw('(acos(cos(RADIANS('.$lat.'))
        *cos(RADIANS(restaurants.lat))
        *cos(RADIANS(restaurants.long)-RADIANS('.$lng.'))+sin(RADIANS('.$lat.'))
        *sin(RADIANS(restaurants.lat)))*6371) as km'))
        ->whereRaw('(acos(cos(RADIANS('.$lat.'))
        *cos(RADIANS(restaurants.lat))
        *cos(RADIANS(restaurants.long)-RADIANS('.$lng.'))+sin(RADIANS('.$lat.'))
        *sin(RADIANS(restaurants.lat)))*6371) < ?',[3])
        ->get();
        if(!empty($data['res'])){
            session()->put('search_res','Không tìm thấy kết quả');
        }
        return view('frontend.restaurants.searchrestaurant',$data);
    }
    public function comment(Request $r,$slug){
        $detail = Restaurant::where('res_slug',$slug)->first();
        $newcomment = new Commentres;
        $newcomment->conntent = $r->comment;
        $newcomment->customer_id = Auth::guard('customer')->id();
        $newcomment->restaurant_id = $detail->id;
        $newcomment->save();
        return redirect()->back();
    }
    public function detailPost($slug)
    {
        $data['detail'] = Newres::where('title_slug',$slug)->first();
        $data['post'] = Newres::paginate(10);
        return view('frontend.restaurants.postrestaurant',$data);
    }
}

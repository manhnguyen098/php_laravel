<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignUpRequest;
use Illuminate\Support\Facades\Auth;
use App\Model\Customer;

class CustomerController extends Controller
{
    public function getLogin()
    {
    	return view('frontend.customer.login');
    }
    public function postLogin(LoginRequest $r)
    {
    	if(Auth::guard('customer')->attempt(['email'=>$r->email,'password'=>$r->password])){
            return redirect('menu');
        }else{
            return redirect('dang-nhap')->with('thongbao','Tài khoản hoặc mật khẩu không chính xác!');
        }
    }
    public function getRegister()
    {
        return view('frontend.customer.register');
    }
    public function postRegister(SignUpRequest $r)
    {
        $customer = new Customer;
        $customer->email = $r->email;
        $customer->password = bcrypt($r->password);
        $customer->name = $r->name;
        $customer->address = $r->address;
        $customer->phone = $r->phone;
        if($r->password == $r->re_pass){
            $customer->save();
            return redirect('dang-nhap')->with('thongbao','Đăng kí thành công vui lòng đăng nhập');
        }else{
            return redirect()->back()->with('thongbao','Xác thực mật khẩu không khớp');
        }
    }
}

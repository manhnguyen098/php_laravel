<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\helper\Cart;

class CartController extends Controller
{
    public function addCart(Cart $cart,$id,$qty)
    {
        $product = Product::find($id);
        
        $cart->add($product,$qty);
       return back();
    }
    public function getCart()
    {   
        // $cart = new Cart;
       $cart = session('cart') ? session('cart') : [];
        return view('frontend.cart.cart',compact('cart'));
    }

    public function deleteItemCart(Cart $cart,$id)
    {   
    	$cart->deleteItem($id);

    	return redirect()->route('getCart');
    }
    public function deleteCart(Cart $cart)
    {
        $cart->deleteCart();
        return back();
    }
    public function update(Cart $cart,Request $request)
    {   
        $quantity = $request->quantity;
        $id = $request->id;
        $cart->update($id,$quantity);
        return back();
    }
}
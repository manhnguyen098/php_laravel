<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Oder;
use App\Model\Oderdetail;
use App\Model\Product;

class CheckOutController extends Controller
{
    public function getCheckout()
    {
    	return view('frontend.cart.checkout');
    }
    	public function postCheckOut(Request $rq)
    {	
    	$this->validate($rq,
    		[	
    			'name' => 'required',
    			'email'=>'required|email',
    			'address'=>'required',
    			'phone'=>'required'
    		],
    		[	
    			'name.required' =>'Vui lòng nhập họ và tên',
    			'email.required'=>'Vui lòng nhập email',
    			'email.email'=>'Email chưa đúng định dang',
    			'address.required' =>'Vui lòng nhập địa chỉ nhận hàng',
    			'phone.required'=>'Vui lòng nhập số điện thoại',
    		]
    	);
    	$total = session('total');
    	$oder = new Oder;
    	$oder->name = $rq->name;
    	$oder->gender = $rq->gender;
    	$oder->email = $rq->email;
    	$oder->phone = $rq->phone;
    	$oder->address = $rq->address;
    	$oder->payment_method = $rq->payment_method;
    	$oder->total = $total;
    	$oder->status = 0;
    	$oder->note = $rq->note;
    	$oder->save();
    	foreach (session('cart') as $item) {
			$product = Product::find($item['id']);
			$sld = $product->sld + $item['quantity'];
        	$product->update(array('sld'=>$sld));
			$oder_detail = new Oderdetail;
    		$oder_detail->order_id = $oder->id;
    		$oder_detail->restaurant_id = $item['id_restuanrent'];
    		$oder_detail->product_name = $item['name'];
    		$oder_detail->product_img = $item['image'];
    		$oder_detail->quantity = $item['quantity'];
			$oder_detail->price = $item['price'];
			$oder_detail->save();
    	}
    	session(['cart'=>'']);
    	session(['total'=>'']);
    	return back()->with('thongbao','Đặt hàng thành công');
    }
}
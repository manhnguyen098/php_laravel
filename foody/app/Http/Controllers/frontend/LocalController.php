<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocalController extends Controller
{
    public function local(Request $r,$local){
            $r->session()->put('local',$local);       
         return redirect()->back();
    }
}

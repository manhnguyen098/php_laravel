<?php

namespace App\Http\Controllers\frontend;
use App\Http\Requests\SiginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class SigninsellController extends Controller
{
    public function signIn(){
        return view('frontend.customer.signin');
    }
    public function postSignIn(SiginRequest $r){
        $user = new User;
        $user->email = $r->email;
        $user->fist_name = $r->fist_name;
        $user->last_name = $r->last_name;
        $user->password = bcrypt($r->password);
        $user->phone = $r->phone;
        $user->address = $r->address;
        if($r->password == $r->re_pass){
            $user->save();
            return redirect('admin');
        }else {
            return redirect()->back()->with('thongbao','Xác thực mật khẩu không khớp');
        }
    }
}

<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Product,Restaurant,Comment};
use DB;
class DiscoverController extends Controller
{
    public function index()
    {
        if(session()->has('local')){
            $local = session('local');
        }else {
            $local = 'Hà Nội';
        }
        
        //do an noi bat
        $food_endow = DB::table('restaurants')->join('products','products.restaurant_id','=','restaurants.id')
        ->where('featured','=',1)->where('city',$local)->paginate(12);
        /// do an uu dai
        $food_sale = DB::table('restaurants')->join('products','products.restaurant_id','=','restaurants.id')
        ->where('sale_off','>',0)->where('city',$local)->paginate(12);
        // do an mua nhieu
        $food_sld = DB::table('restaurants')->join('products','products.restaurant_id','=','restaurants.id')
        ->where('category_id',1)->where('city',$local)->orderBy('sld','desc')->take(50)->paginate(8);
        
        return view('frontend.menu.discover',compact('food_endow','food_sale','food_sld'));
    }
    public function getDetail($prd_slug)
    {
        $pro = Product::where('prd_slug',$prd_slug)->first();
        $product = Product::paginate(10);
        $post = Comment::where('product_id',$pro->id)->get();
        $total = Comment::where('product_id',$pro->id)->count();
        return view('frontend.menu.product_detail',compact('pro','product','post','total'));
    }
    public function searchProduct(Request $r){
        $address = $r->search; 
        $apiKey = 'AIzaSyD7gzwc-HNYPk-Wz8IaiAIfInK0t9p5ISo'; 

        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);
        $geo = json_decode($geo, true); 
        $lat = $geo['results'][0]['geometry']['location']['lat'];
        $lng = $geo['results'][0]['geometry']['location']['lng'];

        $data['prd']=DB::table('restaurants')
        ->select('products.id','address','product_name','product_img',DB::raw('(acos(cos(RADIANS('.$lat.'))
        *cos(RADIANS(restaurants.lat))
        *cos(RADIANS(restaurants.long)-RADIANS('.$lng.'))+sin(RADIANS('.$lat.'))
        *sin(RADIANS(restaurants.lat)))*6371) as km'))
        ->join('products','products.restaurant_id','=','restaurants.id')
        ->whereRaw('(acos(cos(RADIANS('.$lat.'))
        *cos(RADIANS(restaurants.lat))
        *cos(RADIANS(restaurants.long)-RADIANS('.$lng.'))+sin(RADIANS('.$lat.'))
        *sin(RADIANS(restaurants.lat)))*6371) < ?',[3])
        ->paginate(8);
        if(!empty($data['prd'])){
            session()->put('search_prd','Không tìm thấy kết quả');
        }
        return view('frontend.menu.searchproduct',$data);
    }
    public function comment(Request $r,$slug){
        $detail = Product::where('prd_slug',$slug)->first();
        $newcomment = new Comment;
        $newcomment->conntent = $r->comment;
        $newcomment->customer_id = Auth::guard('customer')->id();
        $newcomment->product_id = $detail->id;
        $newcomment->save();
        return redirect()->back();
    }
}

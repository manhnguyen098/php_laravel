<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Commentres extends Model
{
    protected $table ='commentres';
    public function customer()
    {
        return $this->belongsTo('App\Model\Customer');
    }
    public function restaurants()
    {
        return $this->belongsTo('App\Model\Restaurant');
    }
}

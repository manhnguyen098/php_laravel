<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Oderdetail extends Model
{
    protected $table = 'orders_detail';
    protected $fillable = ['id_oder','id_pro','name','restaurent_id','quantity','price'];
}

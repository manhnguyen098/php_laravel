<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    public function customer()
    {
        return $this->belongsTo('App\Model\Customer');
    }
}

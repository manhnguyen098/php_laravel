<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = [
        'product_name','product_img','price','sld','sale_off','id','featured','restaurant_id'
    ];
    public function restaurant()
    {
        return $this->belongsTo('App\Model\Restaurant');
    }
    public function category()
    {
        return $this->belongsTo('App\Model\Category');
    }
}

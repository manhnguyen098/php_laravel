<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Oder extends Model
{
    protected $table = 'orders';
    protected $fillable = ['id','name','gender','email','phone','address','payment_method','total','status','note'];

}

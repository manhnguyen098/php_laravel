<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = "restaurants";
    protected $timestamp = false;
    public function products()
    {
        return $this->hasMany('App\Model\Product','restaurant_id');
    }
    public function new()
    {
        return $this->hasMany('App\Model\Newres','restaurant_id');
    }
    public function comment()
    {
        return $this->hasMany('App\Model\Commentres','restaurant_id');
    }
}

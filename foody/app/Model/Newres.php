<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newres extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'title', 'title_slug', 'content','restaurant_id'
    ];
    public function res()
    {
        return $this->belongsTo('App\Model\Restaurant');
    }
}
